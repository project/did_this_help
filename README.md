# Did this help?

Displays block with `"Did this help"` (is useful) question and Yes/No options.
For "No" options are available list of ready answers and text field for custom
answer. Answers report is available on `/admin/reports/did-this-help`
Module features:

- Views integration
- Configuration form for answers and titles (Drupal 8+ only)
- For Drupal 7 version you can see results on `"Did this help?"` tab for each node separately
  or you can see all result on common `"Did this help?"` views page.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/did_this_help).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/did_this_help).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module does not have any dependency on any other module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## Maintainers

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
